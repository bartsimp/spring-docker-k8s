package com.example.person.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.person.dto.Person;
import com.example.person.exception.NotFoundException;
import com.example.person.model.PersonEntity;
import com.example.person.repository.PersonRepository;

/**
 * @author alessiofiore
 */
@Service
public class PersonService {

    @Autowired
    private PersonRepository repository;

    public List<Person> getAll() {
        List<Person> students = new ArrayList<>();
        repository.findAll().forEach(s -> students.add(new Person(s.getId(), s.getName())));
        return students;
    }

    public String getName(Long id) throws NotFoundException {
        Optional<PersonEntity> opt = repository.findById(id);
        if(opt.isPresent()) {
            return opt.get().getName();
        } else {
            throw new NotFoundException();
        }
    }

    public Person create(Person p) {
        PersonEntity e = new PersonEntity();
        e.setName(p.getName());
        e = repository.save(e);
        Person res = new Person(e.getId(), e.getName());
        return res;
    }
}
