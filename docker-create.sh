#!/bin/bash
#docker create -e PERSON-SERVICE-IP=person-service -e PERSON-SERVICE-PORT=8080 -p 9090:8080 --name hello-service --network spring-services-ntw alessiofiore/spring-k8s-hello-service:1.0.0
#docker create -p 9091:8080 --name person-service --network spring-services-ntw alessiofiore/spring-k8s-person-service:1.0.0

docker network create spring-services-ntw
docker run -d --name mysql-db       --network spring-services-ntw -p 3306:3306 -e MYSQL_ROOT_PASSWORD=password123 -e MYSQL_DATABASE=person_db mysql:latest
sleep 20s
docker run -d --name person-service --network spring-services-ntw -p 8080:8080 -e MYSQL_HOST_POST_DB=mysql-db -e MYSQL_PORT_POST_DB=3306 -e MYSQL_ROOT_PASSWORD=password123 -e SPRING_PROFILE=docker registry.gitlab.com/bartsimp/spring-docker-k8s/bartsimp/person-service
sleep 20s
docker run -d --name hello-service  --network spring-services-ntw -p 9090:8081 -e PORT=8081 -e PERSON_SERVICE_IP=person-service -e PERSON_SERVICE_PORT=8080 registry.gitlab.com/bartsimp/spring-docker-k8s/bartsimp/hello-service
