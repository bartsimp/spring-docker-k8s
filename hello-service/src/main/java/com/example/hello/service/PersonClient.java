package com.example.hello.service;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

/**
 * @author alessiofiore
 */
@FeignClient(name = "person-service-ws", url = "${person-service-ws.ip}:${person-service-ws.port}")
public interface PersonClient {

    @GetMapping(value = "/{id}")
    String getName(@PathVariable(name = "id") Long id);
}
