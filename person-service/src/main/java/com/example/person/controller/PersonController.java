package com.example.person.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.example.person.dto.Person;
import com.example.person.exception.NotFoundException;
import com.example.person.service.PersonService;

import lombok.extern.slf4j.Slf4j;

/**
 * @author alessiofiore
 */
@RestController
@Slf4j
public class PersonController {

    @Value("${instance.instance-id}")
    private String instanceId;

    @Autowired
    private PersonService personService;

    @GetMapping
    public List<Person> getAll() {
        return this.personService.getAll();
    }

    @GetMapping("/{id}")
    public String getName(@PathVariable("id") Long id) {
        log.info("Operation executed by instance [{}]", instanceId);
        try {
            return personService.getName(id);
        } catch (NotFoundException e) {
            return "No name";
        }
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Person create(@RequestBody Person p) {
        return personService.create(p);
    }
}
