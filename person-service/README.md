# spring-boot-docker
Basic Spring Boot application with Docker

## Build package
    mvn package
### Test application
    curl -X GET http://localhost:8080/1
    (bash)  curl -i -S -X POST --header 'Content-Type: application/json' --header 'Accept: application/json' -d ' { "name": "paperino" }' 'http://localhost:8080'
    (msdos) curl -X POST --header "Content-Type: application/json" --header "Accept: application/json" -d "{ \"name\": \"paperino\" }\" http://localhost:8080'

## Docker

### Build image
    docker build -t alessiofiore/spring-k8s-person-service:1.0.0 .
    docker push alessiofiore/spring-k8s-person-service:1.0.0
    
### Create container
    docker create -p 9091:8080 --name person-service --network spring-services-ntw alessiofiore/spring-k8s-person-service:1.0.0
    
### Manage container
    docker start person-service
    docker stop person-service
    docker logs -f person-service
    
### Test application
    GET http://localhost:9091 -> World